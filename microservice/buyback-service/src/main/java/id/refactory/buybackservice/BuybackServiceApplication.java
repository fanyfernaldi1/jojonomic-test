package id.refactory.buybackservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuybackServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuybackServiceApplication.class, args);
	}

}
