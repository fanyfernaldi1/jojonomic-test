package id.refactory.topupstorage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.refactory.topupstorage.entities.AccountNumberEntity;
import id.refactory.topupstorage.entities.TransactionEntity;
import id.refactory.topupstorage.kafka.KafkaMessage;
import id.refactory.topupstorage.repos.AccountNumberRepo;
import id.refactory.topupstorage.repos.TransactionRepo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
public class TopupStorageApplication {

	private final AccountNumberRepo accountNumberRepo;
	private final TransactionRepo transactionRepo;

	public TopupStorageApplication(AccountNumberRepo accountNumberRepo, TransactionRepo transactionRepo) {
		this.accountNumberRepo = accountNumberRepo;
		this.transactionRepo = transactionRepo;
	}

	public static void main(String[] args) {
		SpringApplication.run(TopupStorageApplication.class, args);
	}

	@Transactional
	@KafkaListener(topics = "topup", groupId = "e_gold")
	public void listen(String message) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			KafkaMessage kafkaMessage = objectMapper.readValue(message, KafkaMessage.class);

			Long timeMillis = System.currentTimeMillis();
			String accountNumber = kafkaMessage.getAccountNumber();
			Double currentBalance = kafkaMessage.getGoldBalance() + kafkaMessage.getWeight();

			AccountNumberEntity accountNumberEntity = accountNumberRepo.findByAccountNumber(accountNumber);
			if (accountNumberEntity == null) {
				accountNumberEntity = new AccountNumberEntity();
				accountNumberEntity.setId(timeMillis);
				accountNumberEntity.setAccountNumber(kafkaMessage.getAccountNumber());
				accountNumberEntity.setCreatedAt(timeMillis);
				accountNumberEntity.setBalance(currentBalance);
				accountNumberRepo.save(accountNumberEntity);
			}

			accountNumberRepo.updateBalance(currentBalance, accountNumber);

			TransactionEntity transactionEntity = new TransactionEntity();
			transactionEntity.setBalance(currentBalance);
			transactionEntity.setAccountNumber(accountNumber);
			transactionEntity.setType(kafkaMessage.getType());
			transactionEntity.setWeight(kafkaMessage.getWeight());
			transactionEntity.setTopUpPrice(kafkaMessage.getTopUpPrice());
			transactionEntity.setBuyBackPrice(kafkaMessage.getBuyBackPrice());
			transactionEntity.setCreatedAt(timeMillis);
			transactionEntity.setId(timeMillis);
			transactionRepo.save(transactionEntity);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}
}
