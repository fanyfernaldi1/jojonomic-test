package id.refactory.checkhargaservice.repos;

import id.refactory.checkhargaservice.entities.GoldPriceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GoldPriceRepo extends JpaRepository<GoldPriceEntity, Long> {
    GoldPriceEntity findFirstByOrderByIdDesc();
}
