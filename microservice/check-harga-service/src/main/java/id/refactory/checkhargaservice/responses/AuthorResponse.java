package id.refactory.checkhargaservice.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AuthorResponse {
    @JsonProperty
    private final String name = "Fany Fernaldi";
    @JsonProperty
    private final String github = "https://github.com/fanyfernaldi";
    @JsonProperty
    private final String linkedin = "https://www.linkedin.com/in/fany-fernaldi-989aa1152/";
    @JsonProperty("stack_overflow")
    private final String stackOverflow = "https://stackoverflow.com/users/8875292/fany-fernaldi";

    @JsonCreator
    public AuthorResponse() {

    }
}
