package id.refactory.checksaldoservice.controllers;

import id.refactory.checksaldoservice.entities.AccountNumberEntity;
import id.refactory.checksaldoservice.repos.AccountNumberRepo;
import id.refactory.checksaldoservice.requests.CheckSaldoRequest;
import id.refactory.checksaldoservice.responses.BaseResponse;
import id.refactory.checksaldoservice.responses.SaldoResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "api/")
public class CheckSaldoController {

    private final AccountNumberRepo accountNumberRepo;

    public CheckSaldoController(AccountNumberRepo accountNumberRepo) {
        this.accountNumberRepo = accountNumberRepo;
    }

    @PostMapping(value = "saldo", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    BaseResponse<SaldoResponse> topUp(@RequestBody CheckSaldoRequest checkSaldoRequest) {
        String accountNumber = checkSaldoRequest.getAccountNumber();

        AccountNumberEntity accountNumberEntity = accountNumberRepo.findByAccountNumber(accountNumber);
        SaldoResponse saldoResponse = new SaldoResponse(
                accountNumberEntity.getAccountNumber(),
                accountNumberEntity.getBalance()
        );

        String reffId = "test_001";
        return BaseResponse.success(reffId, saldoResponse);
    }
}
