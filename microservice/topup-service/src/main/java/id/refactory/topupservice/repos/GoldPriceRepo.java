package id.refactory.topupservice.repos;

import id.refactory.topupservice.entities.GoldPriceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GoldPriceRepo extends JpaRepository<GoldPriceEntity, Long> {
    GoldPriceEntity findFirstByOrderByIdDesc();
}
