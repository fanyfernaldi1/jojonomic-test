package id.refactory.inputhargaservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InputHargaServiceApplication {
	public static void main(String[] args) {
		SpringApplication.run(InputHargaServiceApplication.class, args);
	}
}
