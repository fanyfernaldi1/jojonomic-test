package id.refactory.buybackstorage;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.refactory.buybackstorage.entities.TransactionEntity;
import id.refactory.buybackstorage.kafka.KafkaMessage;
import id.refactory.buybackstorage.repos.AccountNumberRepo;
import id.refactory.buybackstorage.repos.TransactionRepo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
public class BuybackStorageApplication {

	private final AccountNumberRepo accountNumberRepo;
	private final TransactionRepo transactionRepo;

	public BuybackStorageApplication(AccountNumberRepo accountNumberRepo, TransactionRepo transactionRepo) {
		this.accountNumberRepo = accountNumberRepo;
		this.transactionRepo = transactionRepo;
	}

	public static void main(String[] args) {
		SpringApplication.run(BuybackStorageApplication.class, args);
	}

	@Transactional
	@KafkaListener(topics = "buyback", groupId = "e_gold")
	public void listen(String message) {
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			KafkaMessage kafkaMessage = objectMapper.readValue(message, KafkaMessage.class);

			Long timeMillis = System.currentTimeMillis();
			String accountNumber = kafkaMessage.getAccountNumber();
			Double currentBalance = kafkaMessage.getGoldBalance() - kafkaMessage.getWeight();

			accountNumberRepo.updateBalance(currentBalance, accountNumber);

			TransactionEntity transactionEntity = new TransactionEntity();
			transactionEntity.setBalance(currentBalance);
			transactionEntity.setAccountNumber(accountNumber);
			transactionEntity.setType(kafkaMessage.getType());
			transactionEntity.setWeight(kafkaMessage.getWeight());
			transactionEntity.setTopUpPrice(kafkaMessage.getTopUpPrice());
			transactionEntity.setBuyBackPrice(kafkaMessage.getBuyBackPrice());
			transactionEntity.setCreatedAt(timeMillis);
			transactionEntity.setId(timeMillis);
			transactionRepo.save(transactionEntity);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}
}
