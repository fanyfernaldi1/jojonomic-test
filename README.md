## Test Programmer Java Jojonomic - Fany

### run docker compose before running this project

```bash
docker-compose -f ./misc/docker-compose.yaml up -d
```

### Video Result Link
https://www.loom.com/share/9436c33cf5a0413e96940e070e27691d

### Postman Collection Location
https://gitlab.com/fanyfernaldi1/jojonomic-test/-/tree/main/misc