package id.refactory.jojonomictest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JojonomicTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(JojonomicTestApplication.class, args);
	}

}
